class TodosController < ApplicationController
  def index
    todos = []

    50.times { todos << Reminder.new.to_message }

    res = Todo::TodoCollectionResponse.new(todos: todos)

    send_data Todo::TodoCollectionResponse.encode(res)
  end

  def show
    todo = Reminder.new.to_message
    res = Todo::TodoResponse.new(todo: todo)

    send_data Todo::TodoResponse.encode(res)
  end

  def create
    todo = Reminder.new.to_message
    res = Todo::CreateTodoResponse.new(todo: todo)

    send_data Todo::CreateTodoResponse.encode(res)
  end
end