class DefectsController < ApplicationController
  # returns a list
  def bulk_create
    # manually created request string
    req = "\n\x05bgfde\x1A5\x12\x05grfed\x1A\x14Scratched Windscreen\"\vStupid bird2\x06\b\xAE\x86\x90\x8F\x068\x90\x1C\x1A5\x12\x05grfed\x1A\x14Scratched Windscreen\"\vStupid bird2\x06\b\xAE\x86\x90\x8F\x068\x90\x1C\x1A5\x12\x05grfed\x1A\x14Scratched Windscreen\"\vStupid bird2\x06\b\xAE\x86\x90\x8F\x068\x90\x1C\x1A5\x12\x05grfed\x1A\x14Scratched Windscreen\"\vStupid bird2\x06\b\xAE\x86\x90\x8F\x068\x90\x1C\x1A5\x12\x05grfed\x1A\x14Scratched Windscreen\"\vStupid bird2\x06\b\xAE\x86\x90\x8F\x068\x90\x1C"
    decoded_request = Aircraft::AddDefectRequest.decode(request.body.read)

    new_defects = decoded_request.defects.map.with_index do |d, i|
      Defect::Defect.new(
        id: i.to_s,
        aircraftId: decoded_request.try(:id) || decoded_request.try(:registration),
        name: d.name,
        description: d.description,
        deferral: Defect::Defect::Deferral.const_get(d.deferral),
        dueDate: d.dueDate,
        dueFlightSeconds: d.dueFlightSeconds
      )
    end

    response_message = Aircraft::AddDefectResponse.new(defects: new_defects)
    encoded_response = Aircraft::AddDefectResponse.encode(response_message)

    send_data encoded_response
  end
end