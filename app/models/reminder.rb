class Reminder
  attr_accessor :due, :message

  FIXABLE_ENDPOINTS = %w[sync logbook me mel_item aircraft].freeze
  DAYS = 1.upto(20).to_a.freeze

    ###
  # Create a model from a message object
  def self.from_message(message)
    new.tap do |reminder|
      reminder.due = message.due
      reminder.message = message.message
    end
  end

  ###
  # Create a message object from model
  def to_message
    due_at = (Time.now + DAYS.sample.day).to_i

    Todo::Todo.new(
      due: Google::Protobuf::Timestamp.new(seconds: due_at),
      message: "Fix the #{FIXABLE_ENDPOINTS.sample} endpoint"
    )
  end

  ###
  # Encode model data in protobuf format
  def serialize
    Todo::Todo.encode(to_message)
  end

  ###
  # Decode protobuf data and hydrate model
  def self.deserialize(data)
    message = Todo::Todo.decode(data)
    from_message(message)
  end
end