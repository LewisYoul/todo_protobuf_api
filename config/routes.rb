Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :todos, only: %i[index show create]
  resources :defects do
    post :bulk_create, on: :collection
  end
end
